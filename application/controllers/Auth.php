<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Auth extends CI_Controller
{

  function __construct()
  {
    parent::__construct();
    $this->load->library('form_validation');
    $this->load->model('Main_model');
    $this->load->helper('url');
  }

  public function index()
  {
    $data['title'] = 'Login | Ethes Gate';
    //load model


    $this->load->view('auth/login', $data);
  }

  function create_alert($message)
  {
    print_r("<script type='text/javascript'>alert('" . $message . "');</script>");
  }

  public function login()
  {
    $this->form_validation->set_rules('user_username', 'Username', 'trim|required');
    $this->form_validation->set_rules('user_password', 'Password', 'trim|required');



    $data['title'] = 'Login | Ethes Gate';

    if ($this->form_validation->run() == false) {
      $this->create_alert("empty");
      $this->load->view('auth/login', $data);
    } else {

      $this->do_login();
    }
  }

  private function do_login()
  {
    $this->load->library('session');
    $user_username = $this->input->post('user_username');
    $user_password = md5($this->input->post('user_password'));
    $user_level = $this->input->post('user_level');


    $data['title'] = 'Login | Ethes Gate';

    $user = $this->db->get_where('tbl_user', ['user_username' => $user_username])->row_array();

    if (!empty($user)) {
      $user_password_db     = $user['user_password'];
      $id_user              = $user['id_user'];
      $nama                 = $user['nama'];
      $user_username        = $user['user_username'];
      $user_level           = $user['user_level'];
      $user_status          = $user['user_status'];

      if ($user_password == $user_password_db) {
        $sesdata = array(
          'id'                => $id_user,
          'user_username'     => $user_username,
          'nama'              => $nama,
          'user_level'        => $user_level,
          'user_status'       => $user_status,
          'logged_in'         => TRUE
        );

        // print_r($user_username);
        // exit();
        $this->session->set_userdata($sesdata);
        if ($user_level == '1') {
          redirect('admin');
        } else if ($user_level == '2') {
          redirect('owner');
        } else {
          redirect('karyawan');
        }
      } else {
        $this->session->set_flashdata('msg', 'Password yang Anda masukkan salah');
        $this->load->view('auth/login', $data);
      }
    } else {
      $this->session->set_flashdata('msg', 'Username tidak ditemukan');
      $this->load->view('auth/login', $data);
    }
  }

  function logout()
  {
    $this->session->sess_destroy();
    redirect('auth');
  }
}
