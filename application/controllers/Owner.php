<?php
class Owner extends CI_Controller
{
  public function __construct()
  {
    parent::__construct();
    if ($this->session->userdata('logged_in') != TRUE) {
      redirect('auth');
    }
    $this->load->model('Main_model');
    $this->load->library('form_validation');
  }

  public function index()
  {

    $data['judul'] = 'Dashboard Owner';
    //  $data['mahasiswa'] = $this->Mahasiswa_model->getAllMahasiswa();


    $this->load->view('templates/header', $data);
    $this->load->view('templates/sidebar', $data);
    $this->load->view('owner/dashboard', $data);
    $this->load->view('templates/footer');
  }
}
