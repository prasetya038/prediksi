<?php
class Admin extends CI_Controller
{
  public function __construct()
  {
    parent::__construct();
    if ($this->session->userdata('logged_in') != TRUE) {
      redirect('auth');
    }
    $this->load->model('Main_model');
    $this->load->library('form_validation');
  }

  public function index()
  {

    $data['judul'] = 'Dashboard Admin';
    //  $data['mahasiswa'] = $this->Mahasiswa_model->getAllMahasiswa();


    $this->load->view('templates/header', $data);
    $this->load->view('templates/sidebar', $data);
    $this->load->view('admin/dashboard', $data);
    $this->load->view('templates/footer');
  }

  public function showDataBarang()
  {
    $data['judul'] = 'Data Barang';
    $data['tbl_barang'] = $this->Main_model->getAllDataBarang();


    $this->load->view('templates/header', $data);
    $this->load->view('templates/sidebar', $data);
    $this->load->view('admin/pages/databarang', $data);
    $this->load->view('templates/footer');
  }

  public function inputDataBarang()
  {
    $barang_id         = $this->input->post('barang_id');
    $barang_nama       = $this->input->post('barang_nama');
    $barang_stok       = $this->input->post('barang_stok');

    $data = array(
      'barang_id'            => $barang_id,
      'barang_nama'          => $barang_nama,
      'barang_stok'          => $barang_stok
    );

    $this->Main_model->input_databarang($data, 'tbl_barang');

    redirect('admin/showDataBarang');
  }

  public function deleteDataBarang($id)
  {
    $this->Main_model->delete_databarang($id);

    redirect('admin/showDataBarang');
  }
}
