<?php

class Main_model extends CI_model
{
  public function getAllDataBarang()
  {
    return $query = $this->db->get('tbl_barang')->result_array();
  }
  public function input_databarang($data, $table)
  {
    return $this->db->insert($table, $data);
  }
  public function delete_databarang($id)
  {
    $this->db->delete('tbl_barang', ['barang_id' => $id]);
  }
}
