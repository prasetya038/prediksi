<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <div class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-6">
          <h1 class="m-0">Data Barang</h1>
        </div><!-- /.col -->
        <div class="col-sm-6">
          <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item"><a href="#">Home</a></li>
            <li class="breadcrumb-item active">Data Barang</li>
          </ol>
        </div><!-- /.col -->
      </div><!-- /.row -->
    </div><!-- /.container-fluid -->
  </div>
  <!-- /.content-header -->

  <!-- Main content -->
  <section class="content">
    <div class="container-fluid">
      <div class="row">
        <div class="col-12">
          <div class="card">
            <div class="card-header">
              <h3 class="card-title">Data Barang Rama Collection</h3>

              <div class="card-tools">
                <div class="input-group input-group-sm" style="width: 150px;">
                  <input type="text" name="table_search" class="form-control float-right" placeholder="Search">

                  <div class="input-group-append">
                    <button type="submit" class="btn btn-default">
                      <i class="fas fa-search"></i>
                    </button>
                  </div>
                </div>
              </div>

            </div>
            <div class="row">
              <div class="col-lg-2 m-2">
                <!-- Button trigger modal -->
                <button type="button" class="btn btn-success" data-toggle="modal" data-target="#exampleModal">
                  Input Data Barang
                </button>

                <!-- Modal -->
                <div class="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
                  <div class="modal-dialog">
                    <div class="modal-content">
                      <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Input Data Barang</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                          <span aria-hidden="true">&times;</span>
                        </button>
                      </div>
                      <div class="modal-body">
                        <?= form_open_multipart('admin/inputDataBarang'); ?>
                        <div class="pl-lg-2">
                          <div class="row">
                            <div class="col-lg-12">
                              <div class="form-group">
                                <label class="form-control-label" for="input-nomor">ID Barang</label>
                                <input type="text" id="barang_id" name="barang_id" class="form-control" placeholder="ID " required>
                              </div>
                            </div>
                            <div class="col-lg-12">
                              <div class="form-group">
                                <label class="form-control-label" for="input-nomor">Nama Barang</label>
                                <input type="text" id="barang_nama" name="barang_nama" class="form-control" placeholder="Nama Barang" required>
                              </div>
                            </div>
                            <div class="col-lg-3">
                              <div class="form-group">
                                <label class="form-control-label" for="input-nomor">Stok</label>
                                <input type="number" id="barang_stok" name="barang_stok" class="form-control" placeholder="Stok" required>
                              </div>
                            </div>
                          </div>
                          <br>
                        </div>
                      </div>
                      <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        <button class="btn btn-icon btn-success" type="submit" name="inputStore">
                          <span class="btn-inner--text">Tambah</span>
                        </button>
                        <?= form_close(); ?>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <!-- /.card-header -->
            <div class="card-body table-responsive p-0">
              <table class="table table-hover text-nowrap">
                <thead>
                  <tr>
                    <th>No</th>
                    <th>ID</th>
                    <th>Nama Barang</th>
                    <th>Jumlah Stok</th>
                    <th>Action</th>
                  </tr>
                  <?php
                  $no = 1;
                  foreach ($tbl_barang as $tbl_barang) :
                  ?>
                </thead>
                <tbody>
                  <tr>
                    <td><?= $no++; ?></td>
                    <td><?= $tbl_barang['barang_id'] ?></td>
                    <td><?= $tbl_barang['barang_nama'] ?></td>
                    <td><?= $tbl_barang['barang_stok'] ?></td>
                    <td><a href="#"><i class="fas fa-edit"></i></a>
                      <a href="#"><i class="fas fa-trash" style="margin-left:10px;"></i></a>
                    </td>
                    <!-- <td>Bacon ipsum dolor sit amet salami venison chicken flank fatback doner.</td> -->
                  </tr>
                </tbody>
              <?php endforeach; ?>
              </table>
            </div>
            <!-- /.card-body -->
          </div>
          <!-- /.card -->
        </div>
      </div>
      <!-- /.col -->
    </div>
    <!-- /.row -->
</div>
<!-- /.container-fluid -->
</section>
<!-- /.content -->
</div>
<!-- /.content -->
</div>
<!-- /.content-wrapper -->